package test

import (
	stringanalyzer "stringAnalizer/internal/stringanalizer"
	"testing"
)

func TestAnalyze(t *testing.T) {
	// Определяем тестовые данные
	tests := []struct {
		input           string
		expectedLetters int
		expectedBytes   int
	}{
		{"hello", 5, 5},
		{"привет", 6, 12},
		{"こんにちは", 5, 15},
		{"你好", 2, 6},
		{"", 0, 0},
	}
	// Проходим по каждому тестовому случаю
	for _, tt := range tests {
		// Вызываем функцию AnalyzeString
		letters, bytes := stringanalyzer.Analyze(tt.input)

		// Проверяем количество букв
		if letters != tt.expectedLetters {
			t.Errorf("AnalyzeString(%q) вернуло %d букв, ожидалось %d", tt.input, letters, tt.expectedLetters)
		}

		// Проверяем количество байт
		if bytes != tt.expectedBytes {
			t.Errorf("AnalyzeString(%q) вернуло %d байт, ожидалось %d", tt.input, bytes, tt.expectedBytes)
		}
	}

}
