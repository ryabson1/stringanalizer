package stringanalyzer

import "unicode/utf8" // Пакет unicode/utf8 используется для работы с UTF-8 строками

func Analyze(word string) (int, int) {
	// Считаем количество букв в слове (rune count)
	letterCount := utf8.RuneCountInString(word)

	// Считаем количество байт в слове
	byteCount := len(word)

	return letterCount, byteCount

}
