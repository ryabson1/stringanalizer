package stringanalyzer

import (
	"fmt"
	"os"
)

// PrintUsage выводит информацию о том, как использовать программу
func PrintUsage() {
	fmt.Println("stringAnalyzer - программа для анализа строки.")
	fmt.Println()
	fmt.Println("Использование:")
	fmt.Println("  stringAnalyzer <word>")
	fmt.Println()
	fmt.Println("Аргументы:")
	fmt.Println("  <word>    Слово для анализа, выводит количество букв и количество байт.")
	fmt.Println()
	fmt.Println("Опции:")
	fmt.Println("  --help    Выводит это сообщение.")
}

// RunAnalyzer проверяет количество аргументов командной строки, вызывает AnalyzeString и выводит результаты
func RunAnalyzer() {

	// Если аргумент --help, выводим сообщение с информацией о программе
	if len(os.Args) == 2 && os.Args[1] == "--help" {
		PrintUsage()
		return
	}

	// Проверяем количество аргументов командной строки
	if len(os.Args) != 2 {
		fmt.Println("Неверное количество аргументов")
		PrintUsage()
		return
	}

	// Получаем слово из аргументов командной строки
	word := os.Args[1]

	// Вызываем функцию AnalyzeString для анализа строки
	letterCount, byteCount := Analyze(word)

	// Выводим результаты
	fmt.Printf("Количество букв: %d\n", letterCount)
	fmt.Printf("Количество байт: %d\n", byteCount)

}
