# stringAnalizer

'stringAnalizer' - программа для анализа строки, написанная на Go. Она принимает слово в качестве аргумента командной строки и выводит количество букв и количество байт, занятых этим словом.

## Использование

```sh
stringAnalyzer <word>
```

**Аргументы**

- '<word>' - Слово для анализа, выводит количество букв и количество байт.

**Опции**

'--help' - Выводит это сообщение
